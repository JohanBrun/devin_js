Boucle While : while(arguments){} ou la boucle continue d'exécuter ce qui est entre accolades tant que l'argument n'est pas satisfait.

conditionnelle if : if(argument){} ou si l'argument est rempli, le bloc de code entre accolades est exécuté, peut être suivi d'un else pour le cas ou l'argument n'est pas satisfait. 

variable : se déclare avec le mot clé let (peut aussi l'être par le biais de var mais let est préférable car déclare la variable au niveau du bloc de code)

random : Math.random permet de générer en pseudo aléatoire un nombre réel compris entre 0 et 1. En le multipliant par le nombre max que l'on souhaite puis en l'arrondissant à l'entier le plus proche avec Math.floor on génère un nombre pseudo-aléatoire entre 0 et le nombre souhaité.

Prompt permet d'afficher un pop-up dans lequel l'utilisateur peut saisir un message.
Console.log affiche dans la console ce qu'on lui demande
Alert permet d'afficher un pop-up.
