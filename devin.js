function jeu_devin(){

    console.log("Voici le jeu du Devin ! Tu dois deviner un nombre choisi par l'ordinateur !")
    let nombre_ordi = Math.floor(Math.random() * 101)
    let proposition = -1
    let compteur = 0

    function devin(propostion, nombre_ordi, compteur) {
        if (propostion == nombre_ordi) {
            return (`Bravo ! gagné en ${compteur} coups !`)
        }
        else {
            return (proposition < nombre_ordi ? 'Trop petit !' : 'Trop grand !')
        }
    }

    while (proposition != nombre_ordi) {
        compteur += 1;
        proposition = prompt('Choisi un nombre entre 0 et 100 : ')
        console.log(devin(proposition, nombre_ordi, compteur))
    }
}